from documentParser import read_qas
import asyncio
import websockets
from qa import QADatabase
import logging

logger = logging.getLogger(__name__)
logger.setLevel(10)  # for debugging
logging.basicConfig()
q, a = read_qas("questionsDatabase.txt")
database = QADatabase(q, a)

async def answerQuestion(websocket, path):
    while True:
        query = await websocket.recv()
        
        r = database.findSimilarQuestion(query)
        if r[0] >= 0.75:
            t = r[1]
        else:
            t = "I'm sorry, but i couldn't understand your question. Can you try again, please?"
        logger.info("Client asked: %s" % (query))

        await websocket.send(t)

async def main():
    async with websockets.serve(answerQuestion, "0.0.0.0", 9000):
        await asyncio.Future()  # run forever

asyncio.run(main())