import nltk
import string
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag
import numpy as np
from scipy import spatial

class QADatabase:
    QRaw = []
    QVect = [[]]
    QStemWords = []
    A = []
    
    def findSimilarQuestion(self, rawQ):
        Q = self.vectorizeQuestion(self.preprocessQuestion(rawQ))
        o = [0 for asdf in range(len(self.QVect))]
        for e in range(len(self.QVect)):
            o[e] = 1 - spatial.distance.cosine(self.QVect[e], Q)
        d = np.argmax(o)
        return (o[d],self.A[d])
    
    def preprocessQuestion(self, Q):
        return [(PorterStemmer().stem(x[0]), x[1]) for x in nltk.pos_tag(nltk.word_tokenize("".join([char for char in Q if char not in string.punctuation]))) if x[0] not in stopwords.words('english')]
    
    def vectorizeQuestion(self, Q):
        vect = [0 for x in range(len(self.QStemWords))]
        for word in Q:
            try:
                vect[self.QStemWords.index(word)]+=1
            except ValueError:
                pass
        return np.array(vect)
        
    def processDict(self):
        a = [self.preprocessQuestion(f) for f in self.QRaw]
        for b in a:
            for f in b:
                if f not in self.QStemWords:
                    self.QStemWords.append(f)
        self.QStemWords.sort()
        self.QVect = np.array([self.vectorizeQuestion(f) for f in a])
    
    def addSingleQA(self, question, answer, thenProcess = False):
        self.QRaw.append(question)
        self.A.append(answer)
        if (thenProcess):
            self.processDict()
            
    def addBulkQA(self, rawQList, rawAList, thenProcess = False):
        for qa in range(len(rawQList)):
            self.addSingleQA(rawQList[qa], rawAList[qa], False)
        if (thenProcess):
            self.processDict()
            
    def __init__ (self, rawQList, rawAList):
        self.addBulkQA(rawQList, rawAList, thenProcess = True)