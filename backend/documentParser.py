
def read_qas(fileName):
    qaList = [[], []]
    
    with open(fileName, 'r') as f:
        for line in f:
            if line[:2] == "Q:":
                qaList[0].append(line[2:-1])
                qaList[1].append("")
            else:
                qaList[1][-1] += line
            
    return qaList