const webSocketsServerPort = 8000;
const webSocketServer = require('websocket').server;
const WebSocket = require('websocket').w3cwebsocket
const https = require('http');
// Commented out due to personal computer not being able to install self-signed certs, which hampers testing.
//const fs = require('fs');
//const options = {
//	key: fs.readFileSync('key.pem'),
//	cert: fs.readFileSync('cert.pem')
//}
const server = https.createServer(/*options*/);
server.listen(webSocketsServerPort);
const wsServer = new webSocketServer({
  httpServer: server
});

const clients = {};

const getUniqueID = () => {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4();
};

wsServer.on('request', function(request) {
  var userID = getUniqueID();
  console.log((new Date()) + ' Recieved a new connection from origin ' + request.origin + ' for protocol ' + JSON.stringify(request.requestedProtocols) + '.');
  const connection = request.accept(null, request.origin);
  clients[userID] = connection;
  console.log('connected: ' + userID + ' in ' + Object.getOwnPropertyNames(clients))
  connection.on('close', function(connection) {
    console.log((new Date()) + " Peer " + userID + " disconnected.");
    delete clients[userID];
  });
  
  var ws;
  try {
	ws = new WebSocket(process.env.BACKEND_LINK);
  } catch {
	try {
	  ws = new WebSocket("ws://localhost:9000/");
	} catch(err) {
		console.log(err)
	  connection.drop(1014, err);
	  return;
	}
  }
  ws.onopen = function() {
    console.log((new Date()) + " Connection to backend for " + userID + " opened.");
  };
  ws.onmessage = function(e) {
	clients[userID].send(e.data);
    console.log((new Date()) + " Message sent: " + e.data + ".");
  };
  ws.onclose = function(e) {
  console.log((new Date()) + " Connection to backend for " + userID + " disconnected. ("+JSON.stringify(e)+")");
	connection.close();
  };
  connection.on('message', function(message) {
    console.log((new Date()) + " Message received: " + message.utf8Data + ".");
	ws.send(message.utf8Data);
  });
});
