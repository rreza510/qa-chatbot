import './message.css';
const Messages = ({messages}) => {
	
	return (
		<>
			{messages.map((message, i) => { return (
				<div key={i} className={message.isUser?'Question':'Answer'}>{message.text}</div>
			)} )}
		</>
	)
}
export default Messages