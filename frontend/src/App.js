import './App.css';
import Messages from './message';
import {useState} from 'react'
var ws;
 
	
	function init (msg, setMsg) {
		if (ws === undefined) {
			try {
				ws = new WebSocket(process.env.API_LINK);
			} catch {
				try {
					ws = new WebSocket("ws://localhost:8000/");
				} catch(err) {
					console.log(JSON.stringify(err))
					return;
				}
			}
		}
		
		ws.onopen = function() {
			output(msg, setMsg, "Hello there, customer. What can i answer for you?");
		};
	  
		ws.onmessage = function(e) {
			output(msg, setMsg, e.data);
		};
	  
		ws.onclose = function(e) {
			output(msg, setMsg, "We have decided to disconnect for now. Thank you for using our service.");
		};

		ws.onerror = function(e) {
			output(msg, setMsg, "ERROR: "+JSON.stringify(e)); 
		};

	}

	function onSubmit (msg, setMsg, {input}) {
		ws.send(input);
		output(msg, setMsg, input, true);
	}

	function output (msg, setMsg, str, isUser = false) {
		const nextMsg = {'text':str, 'isUser':isUser}
		setMsg([...msg, nextMsg]);
	}

function App() {
	const [a, setA] = useState('')
	const [msg, setMsg] = useState([])
	return (
		<div className="App" onLoad={init(msg, setMsg)}>
			<header className="App-header" >
				<Messages messages={msg}/>
			</header>
			<form className="App-footer" onSubmit= {(e) => {e.preventDefault(); onSubmit(msg, setMsg, {'input':a}); setA('')}}>
				<input type="text" value={a} onChange={(e) => setA(e.target.value)} placeholder="Type your message..." disabled = {ws.readyState !== 1}/> 
				<input type="submit" value="Send" disabled = {ws.readyState !== 1}/>
			</form>
			
		</div>
    );
}

export default App;
